package controllers

import (
	"net/http"
	"travisroad/go-todo/model"
	"travisroad/go-todo/utils/token"

	"github.com/gin-gonic/gin"
)

func CurrentUser(c *gin.Context) {
	uid, err := token.ExtractTokenID(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	u, err := model.GetUserById(uid)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	u.PrepareDTO() // delete password

	c.JSON(http.StatusOK, gin.H{"message": "success", "data": u})
}
