package controllers_test

import (
	"bytes"
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"os/exec"
	"testing"
	"travisroad/go-todo/model"
	"travisroad/go-todo/route"
	"travisroad/go-todo/utils/token"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/joho/godotenv"
	"github.com/stretchr/testify/assert"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func TestMain(m *testing.M) {
	connectDB()

	// init data
	var users []*model.User
	for i := gofakeit.IntRange(15, 25); i > 0; i-- {
		u := &model.User{
			Username: gofakeit.Username(),
			Password: gofakeit.Password(true, true, true, true, false, gofakeit.IntRange(8, 16)),
		}
		users = append(users, u)
	}
	users = append(users, &model.User{
		Username: "foo",
		Password: "bar",
	}) // for login test
	if err := model.DB.Save(&users).Error; err != nil {
		log.Fatal(err)
	}

	var tables []*model.Table
	for i := gofakeit.IntRange(50, 100); i > 0; i-- {
		table := &model.Table{
			Name:   gofakeit.Word(),
			UserId: users[gofakeit.IntRange(0, len(users)-1)].ID,
		}
		tables = append(tables, table)
	}
	if err := model.DB.Save(&tables).Error; err != nil {
		log.Fatal(err)
	}

	var todos []*model.TodoItem
	for i := gofakeit.IntRange(400, 500); i > 0; i-- {
		var tid, uid uint

		if rand.Int()%100 > 80 {
			tid = 0
			uid = users[gofakeit.IntRange(0, len(users)-1)].ID
		} else {
			idx := gofakeit.IntRange(0, len(tables)-1)
			tid = tables[idx].ID
			uid = tables[idx].UserId
		}

		todo := &model.TodoItem{
			Content: gofakeit.Sentence(gofakeit.IntRange(40, 100)),
			UserId:  uid,
			TableId: tid,
		}
		todos = append(todos, todo)
	}
	if err := model.DB.Save(&todos).Error; err != nil {
		log.Fatal(err)
	}

	if err := godotenv.Load("../.env.test"); err != nil {
		log.Fatal("Error loading ../.env.test file")
	}
	m.Run()
}

func connectDB() {
	sqlfile := "/tmp/sqlmock_db_0.sqlite"
	exec.Command("rm", "-f", sqlfile).Run()

	gdb, err := gorm.Open(sqlite.Open(sqlfile), &gorm.Config{})
	if err != nil {
		log.Fatalf("an error '%s' was not expected when opening a stub gorm DB connection", err)
	}
	model.Connect(gdb)
}

func TestLogin(t *testing.T) {
	r := route.SetupRouter()

	req, err := http.NewRequest("POST",
		"/api/auth/login",
		bytes.NewReader([]byte(`{"username": "foo", "password": "bar"}`)))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	r.ServeHTTP(rr, req)

	// assert.Equal(t, rr.Code, http.StatusOK)
	if status := rr.Code; status != http.StatusOK {
		t.Fatalf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	var data map[string]interface{}

	if err := json.NewDecoder(rr.Body).Decode(&data); err != nil {
		t.Fatal("response body is not a json")
	}

	t.Log(data)
	tokenString, ok := data["token"].(string)
	if !ok {
		t.Fatal("there is no \"token\" field")
	}

	if err := token.TokenStringValid(tokenString); err != nil {
		t.Fatal(err)
	}
}

func TestLoginFail(t *testing.T) {
	r := route.SetupRouter()

	req, err := http.NewRequest("POST",
		"/api/auth/login",
		bytes.NewReader([]byte(`{"username": "foobarxx", "password": "bar"}`)))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	r.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusBadRequest)
	}

	var data map[string]interface{}

	if err := json.NewDecoder(rr.Body).Decode(&data); err != nil {
		t.Fatal("response body is not a json")
	}

	_, ok := data["error"].(string)
	if !ok {
		t.Fatal("there is no \"error\" field")
	}
}

func TestRegister(t *testing.T) {
	r := route.SetupRouter()

	req, err := http.NewRequest("POST",
		"/api/auth/register",
		bytes.NewReader([]byte(`{"username": "foofoo", "password": "bar"}`)))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	r.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	var users []model.User
	if err := model.DB.Where("username = ?", "foofoo").Find(&users).Error; err != nil {
		t.Fatal(err)
	}
	if len(users) == 0 {
		t.Fatal("there is no user")
	}

	var data map[string]interface{}

	if err := json.NewDecoder(rr.Body).Decode(&data); err != nil {
		t.Fatal("response body is not a json")
	}

	message, ok := data["message"].(string)
	if !ok {
		t.Fatal("there is no \"error\" field")
	}
	assert.Equal(t, message, "success")
}
