package controllers

import (
	"errors"
	"net/http"
	"strconv"
	"travisroad/go-todo/model"
	"travisroad/go-todo/utils"

	"github.com/gin-gonic/gin"
)

type InputTodoItem struct {
	Content string `json:"content" binding:"required"`
}

func getCurrentUserID(c *gin.Context) (uint, error) {
	uid := c.GetUint("uid")
	if uid == 0 {
		err := errors.New("unauthorized uid does not exist")
		return 0, err
	}
	return uid, nil
}

func AddTodoItem(c *gin.Context) {
	var todo InputTodoItem
	if err := c.ShouldBindJSON(&todo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	t := model.TodoItem{}

	t.Content = todo.Content

	uid, err := getCurrentUserID(c)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": err})
	}
	t.UserId = uid
	t.TableId = 0

	_, err = t.SaveTodoItem()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "success", "data": t})
}

func DeleteTodoItem(c *gin.Context) {
	uid, err := getCurrentUserID(c)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": err})
	}

	tid, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	todo, err := model.GetTodoItemByID(uint(tid))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if todo.UserId != uid {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
		return
	}

	if err := model.DeleteTodoItemByID(uint(tid), uid); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "success"})
}

func UpdateTodoItem(c *gin.Context) {
	uid, err := getCurrentUserID(c)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": err})
	}

	var t InputTodoItem
	if err := c.ShouldBindJSON(&t); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	tid, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	todo, err := model.GetTodoItemByID(uint(tid))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	if todo.UserId != uid {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
		return
	}
	todo.Content = t.Content

	if _, err := todo.SaveTodoItem(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "success"})
}

// get a todo by tid
func GetTodoItem(c *gin.Context) {
	uid, err := getCurrentUserID(c)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": err})
	}

	tid, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	todo, err := model.GetTodoItemByID(uint(tid))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	if todo.UserId != uid {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "success", "data": todo})
}

const defaultPageSize = 10
const defaultPageNumber = 0

// 分页参数, 默认每页10条
// GetAllTodoItems retrieves all todo items for a given user ID, with optional pagination parameters.
// It returns a JSON response containing a success message and the requested todo items.
//
// Parameters:
// - c: gin.Context - the HTTP request context
// - size: int - the page size, defaults to 10 if not provided or is invalid, will be clamped to (1, 100)
// - page: int - the page number, defaults to 0 if not provided or is invalid
func GetAllTodoItems(c *gin.Context) {
	uid, err := getCurrentUserID(c)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": err})
	}

	// Get the page size and page number from the request query parameters
	size, err := strconv.Atoi(c.Query("size"))
	if err != nil {
		size = defaultPageSize // Default to 10 if the size parameter is not provided or is invalid
	}
	size = utils.Clamp(size, 1, 100)

	page, err := strconv.Atoi(c.Query("page"))
	if err != nil {
		page = defaultPageNumber // Default to 0 if the page parameter is not provided or is invalid
	}
	if page < 0 {
		page = 0
	}

	// Get the todo items for the given user ID and pagination parameters using the model package
	totalRecord, todos, err := model.GetTodoItemsByUid(uid, page, size)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Return a JSON response containing the success message and the requested todo items
	c.JSON(http.StatusOK,
		gin.H{"message": "success",
			"data":  todos,
			"page":  page,
			"size":  size,
			"total": totalRecord})
}
