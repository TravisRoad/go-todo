package controllers_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"travisroad/go-todo/controllers"
	"travisroad/go-todo/model"
	"travisroad/go-todo/route"
	"travisroad/go-todo/utils/token"

	"github.com/stretchr/testify/assert"
)

/*
registerAndLogin generates a random user with non-zero number of TodoItems
and generates a JWT token for the user. It returns the user ID and the token.

Returns:
- uint: The ID of the registered user.
- string: The JWT token generated for the user.
*/
func registerAndLogin() (uint, string) {
	var users []model.User
	var len int64 = 0
	var uid uint = 0
	// 确保用户一定有数据
	if err := model.DB.Order("RANDOM()").Find(&users).Error; err != nil {
		log.Fatal(err)
	}
	for _, user := range users {
		if err := model.DB.Model(&model.TodoItem{}).Where("user_id = ?", user.ID).Count(&len).Error; err != nil {
			log.Fatal(err)
		}
		if len != 0 {
			uid = user.ID
			break
		}
	}

	token, err := token.GenerateToken(uid)
	if err != nil {
		log.Fatal(err)
	}

	return uid, token
}

// TestAddTodoItem tests the /api/todo endpoint for adding a new todo item
func TestAddTodoItem(t *testing.T) {
	// create input todo item
	inputTodo := &controllers.InputTodoItem{Content: "hello world"}

	// register and login user to get authentication token
	userID, token := registerAndLogin()

	// create router for testing
	router := route.SetupRouter()

	// marshal input todo item to JSON for request body
	requestBody, err := json.Marshal(inputTodo)
	if err != nil {
		t.Fatal("json marshal error")
	}

	// save the number of todo items before adding a new one
	var todoNum int64
	if err := model.DB.Model(&model.TodoItem{}).Where("user_id = ?", userID).Count(&todoNum).Error; err != nil {
		t.Fatal(err)
	}

	// create HTTP request for adding a new todo item
	req, err := http.NewRequest("POST", "/api/todo", bytes.NewBuffer(requestBody))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Authorization", "Bearer "+token)
	recorder := httptest.NewRecorder()

	// make request and record response
	router.ServeHTTP(recorder, req)

	// check if response status code is OK
	if status := recorder.Code; status != http.StatusOK {
		t.Fatalf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	// check if a new todo item was added to the database
	var todoNumNew int64
	if err := model.DB.Model(&model.TodoItem{}).Where("user_id = ?", userID).Count(&todoNumNew).Error; err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, todoNum+1, todoNumNew)

	// parse response body JSON and check if it contains a success message
	var data map[string]interface{}
	if err := json.NewDecoder(recorder.Body).Decode(&data); err != nil {
		t.Fatal("response body is not a json")
	}
	message, ok := data["message"].(string)
	if !ok {
		t.Fatal("there is no \"error\" field")
	}
	assert.Equal(t, message, "success")
}

// test delete todoitem function
func TestDeleteTodoItem(t *testing.T) {
	uid, token := registerAndLogin()

	// create todo
	var todoItem model.TodoItem
	if err := model.DB.Where("user_id = ?", uid).Order("RANDOM()").Take(&todoItem).Error; err != nil {
		t.Fatal(err)
	}

	var todo_num int64
	if err := model.DB.Model(&model.TodoItem{}).Where("user_id = ?", uid).Count(&todo_num).Error; err != nil {
		t.Fatal(err)
	}

	// delete req
	req, err := http.NewRequest("DELETE",
		"/api/todo/"+fmt.Sprint(todoItem.ID), bytes.NewBuffer([]byte{}))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Authorization", "Bearer "+token)
	rr := httptest.NewRecorder()

	r := route.SetupRouter()
	r.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	var todo_new_num int64
	if err := model.DB.Model(&model.TodoItem{}).Where("user_id = ?", uid).Count(&todo_new_num).Error; err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, todo_new_num+1, todo_num)

	var data map[string]interface{}
	if err := json.NewDecoder(rr.Body).Decode(&data); err != nil {
		t.Fatal("response body is not a json")
	}
	message, ok := data["message"].(string)
	if !ok {
		t.Fatal("there is no \"error\" field")
	}
	assert.Equal(t, message, "success")
}

func TestDeleteTodoItem_Fail(t *testing.T) {
	uid, token := registerAndLogin()

	// create todo
	var todoItem model.TodoItem
	if err := model.DB.Where("user_id = ?", uid).Order("RANDOM()").Take(&todoItem).Error; err != nil {
		t.Fatal(err)
	}

	// save the old len of the todos
	var old_len int64
	if err := model.DB.Model(&model.TodoItem{}).Where("user_id = ?", uid).Count(&old_len).Error; err != nil {
		t.Fatal(err)
	}

	// delete a todo that does not exist
	req, err := http.NewRequest("DELETE",
		fmt.Sprintf("/api/todo/%v", 1000), bytes.NewBuffer([]byte{}))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Authorization", "Bearer "+token)
	rr := httptest.NewRecorder()

	r := route.SetupRouter()
	r.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusBadRequest)
	}

	// save the old len of the todos
	var new_len int64
	if err := model.DB.Model(&model.TodoItem{}).Where("user_id = ?", uid).Count(&new_len).Error; err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, old_len, new_len)

	var data map[string]interface{}
	if err := json.NewDecoder(rr.Body).Decode(&data); err != nil {
		t.Fatal("response body is not a json")
	}
	errorMsg, ok := data["error"].(string)
	if !ok {
		t.Fatal("there is no \"error\" field")
	}
	assert.Equal(t, errorMsg, "record not found")
}

func TestUpdateTodoItem(t *testing.T) {
	uid, token := registerAndLogin()

	// create todo
	var todoItem model.TodoItem
	if err := model.DB.Where("user_id = ?", uid).Order("RANDOM()").Take(&todoItem).Error; err != nil {
		t.Fatal(err)
	}

	updateContent := "hello foo bar"
	req, err := http.NewRequest("POST",
		"/api/todo/"+fmt.Sprint(todoItem.ID), bytes.NewBuffer([]byte(fmt.Sprintf(`{"content":"%v"}`, updateContent))))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Authorization", "Bearer "+token)
	rr := httptest.NewRecorder()

	r := route.SetupRouter()
	r.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	var todos []model.TodoItem
	if err := model.DB.Where("id = ?", todoItem.ID).Find(&todos).Error; err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, len(todos), 1)
	assert.Equal(t, todos[0].Content, updateContent)

	var data map[string]interface{}
	if err := json.NewDecoder(rr.Body).Decode(&data); err != nil {
		t.Fatal("response body is not a json")
	}
	message, ok := data["message"].(string)
	if !ok {
		t.Fatal("there is no \"error\" field")
	}
	assert.Equal(t, message, "success")
}

func TestGetAllTodoItem(t *testing.T) {
	uid, token := registerAndLogin()

	var todos []model.TodoItem
	if err := model.DB.Where("user_id = ?", uid).Find(&todos).Error; err != nil {
		t.Fatal(err)
	}

	size, page := 10, 5
	req, err := http.NewRequest("GET", fmt.Sprintf("/api/todo?size=%d&page=%d", size, page), bytes.NewBuffer([]byte{}))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Authorization", "Bearer "+token)
	rr := httptest.NewRecorder()

	r := route.SetupRouter()
	r.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Fatalf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	type Response struct {
		Message string           `json:"message"`
		Data    []model.TodoItem `json:"data"`
		Total   int64            `json:"total"`
		Page    int              `json:"page"`
		Size    int              `json:"size"`
	}

	var rspData Response
	if err := json.NewDecoder(rr.Body).Decode(&rspData); err != nil {
		t.Fatal("response body is not a json")
	}
	assert.Equal(t, rspData.Message, "success")
	assert.GreaterOrEqual(t, size, len(rspData.Data))
	t.Log(len(rspData.Data))

	var totalNum int64
	if err := model.DB.Model(&model.TodoItem{}).Where("user_id = ?", uid).Count(&totalNum).Error; err != nil {
		t.Fatal("db count error")
	}
	assert.Equal(t, totalNum, rspData.Total)

	hashmap := make(map[uint]*model.TodoItem)
	for _, todo := range todos {
		hashmap[todo.ID] = &todo
	}
	for _, todo := range rspData.Data {
		val, ok := hashmap[todo.ID]
		if !ok {
			t.Fatal("todo record not found")
		}
		assert.Equal(t, val.Content, todo.Content)
		assert.Equal(t, val.ID, todo.ID)
		assert.Equal(t, val.UserId, todo.UserId)
		assert.Equal(t, val.TableId, todo.TableId)
	}
}
