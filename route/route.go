package route

import (
	"travisroad/go-todo/controllers"
	"travisroad/go-todo/middlewares"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func SetupRouter() *gin.Engine {
	r := gin.Default()

	// Enable CORS
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	config.AllowHeaders = append(config.AllowHeaders, "Authorization")

	CORSMiddleWare := cors.New(config)
	r.Use(CORSMiddleWare)

	api := r.Group("/api/auth")
	api.POST("/register", controllers.Register)
	api.POST("/login", controllers.Login)

	// user
	user := r.Group("/api/user")
	user.Use(middlewares.JwtAuthMiddleWare())
	user.GET("/me", controllers.CurrentUser)

	// todoItem
	todo := r.Group("/api/todo")
	todo.Use(middlewares.JwtAuthMiddleWare())
	todo.GET("/:id", controllers.GetTodoItem)
	todo.POST("/:id", controllers.UpdateTodoItem)
	todo.DELETE("/:id", controllers.DeleteTodoItem)
	todo.GET("", controllers.GetAllTodoItems)
	todo.POST("", controllers.AddTodoItem)

	return r
}
