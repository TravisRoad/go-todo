package main

import (
	"travisroad/go-todo/model"
	"travisroad/go-todo/route"

	_ "github.com/joho/godotenv/autoload"
)

func main() {
	model.ConnectDB()
	r := route.SetupRouter()
	r.Run()
}
