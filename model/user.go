package model

import (
	"html"
	"strings"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Username string `gorm:"size:255;not null;unique" json:"username"`
	Password string `gorm:"size:255;not null;" json:"password"`
	Role     string `gorm:"size:255;not null;default:default" json:"role"`
}

func (u *User) SaveUser() (*User, error) {
	err := DB.Save(u).Error
	if err != nil {
		return &User{}, err
	}
	return u, nil
}

func (u *User) BeforeSave(db *gorm.DB) error {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	u.Password = string(hashedPassword)
	u.Username = html.EscapeString(strings.TrimSpace(u.Username))

	return nil
}

func VerifyPassword(password, hashedPassword string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

func GetUserById(uid uint) (*User, error) {
	var user User
	if err := DB.First(&user, uid).Error; err != nil {
		return &User{}, err
	}

	return &user, nil
}

// prepare the object before transfer
func (u *User) PrepareDTO() {
	u.Password = ""
}

func LoginCheck(username, password string) (uint, error) {
	var err error
	u := User{}
	err = DB.Model(&u).Where("username = ?", username).Take(&u).Error
	if err != nil {
		return 0, err
	}

	err = VerifyPassword(password, u.Password)

	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		return 0, err
	}

	return u.ID, nil
}

func DeleteUserByID(uid uint) error {
	return DB.Delete(&User{}, uid).Error
}
