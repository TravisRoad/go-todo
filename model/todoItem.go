package model

import "gorm.io/gorm"

type TodoItem struct {
	gorm.Model
	Content string `gorm:"not null" json:"content"`
	UserId  uint   `gorm:"not null" json:"user_id"`
	TableId uint   `gorm:"not null;default:0" json:"table_id"`
}

func (t *TodoItem) SaveTodoItem() (*TodoItem, error) {
	err := DB.Save(t).Error
	if err != nil {
		return &TodoItem{}, err
	}
	return t, nil
}

func DeleteTodoItemByID(tid uint, uid uint) error {
	return DB.Delete(&TodoItem{}, tid).Where("user_id = ?", uid).Error
}

func GetTodoItemByID(tid uint) (*TodoItem, error) {
	var todoItem TodoItem
	err := DB.Where("id = ?", tid).Take(&todoItem).Error
	if err != nil {
		return &TodoItem{}, err
	}
	return &todoItem, nil
}

func GetTodoItemsByUid(uid uint, pageNum, pageSize int) (int64, []*TodoItem, error) {
	var todoItems []*TodoItem
	var totalNum int64
	if err := DB.Where("user_id = ?", uid).Offset((pageNum - 1) * pageSize).Limit(pageSize).Find(&todoItems).Error; err != nil {
		return 0, []*TodoItem{}, err
	}
	if err := DB.Model(&TodoItem{}).Where("user_id = ?", uid).Count(&totalNum).Error; err != nil {
		return 0, []*TodoItem{}, err
	}
	return totalNum, todoItems, nil
}

// Get todoItems by user_id and table_id
func GetTodoItemsByUidAndTid(uid, tid uint, pageNum, pageSize int) ([]*TodoItem, error) {
	var todoItems []*TodoItem
	err := DB.Where("user_id = ? AND table_id = ?", uid, tid).Find(&todoItems).Offset((pageNum - 1) * pageSize).Limit(pageSize).Error
	if err != nil {
		return []*TodoItem{}, err
	}
	return todoItems, nil
}
