package model

import "gorm.io/gorm"

type Table struct {
	gorm.Model
	Name   string `gorm:"not null" json:"name"`
	UserId uint   `gorm:"not null" json:"user_id"`
}

func (t *Table) SaveTable() (*Table, error) {
	err := DB.Save(t).Error
	if err != nil {
		return &Table{}, err
	}
	return t, nil
}

func DeleteTableByID(tid uint) error {
	if err := DB.Model(&TodoItem{}).Where("table_id = ?", tid).Update("table_id", 0).Error; err != nil {
		return err
	}
	return DB.Delete(&Table{}, tid).Error
}
