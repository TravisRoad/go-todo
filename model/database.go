package model

import (
	"fmt"
	"log"
	"os"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var DB *gorm.DB

func ConnectDB() {
	var err error

	//TODO: add postgreSQL

	// connect to sqlite database
	sqliteDBFile, exist := os.LookupEnv("SQLITE_DB_FILE")
	if !exist {
		sqliteDBFile = "db.sqlite"
	}
	DB, err = gorm.Open(sqlite.Open(sqliteDBFile), &gorm.Config{})

	if err != nil {
		log.Fatal("Error connecting to database: ", err)
	} else {
		fmt.Println("Connected to database")
	}

	DB.AutoMigrate(User{}, Table{}, TodoItem{})
}

func Connect(db *gorm.DB) {
	DB = db
	DB.AutoMigrate(User{}, Table{}, TodoItem{})
}

// func init() {
// 	connectDB()
// }
