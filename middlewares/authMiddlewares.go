package middlewares

import (
	"net/http"
	"travisroad/go-todo/utils/token"

	"github.com/gin-gonic/gin"
)

/*
JwtAuthMiddleWare is a middleware function that extracts the user ID from a JWT token in the HTTP request header.
If the token is not found or invalid, it returns an HTTP error response and aborts the request.
Once the user ID is extracted, it is added to the Gin context with the key "uid".
The next middleware in the chain is then called.

Parameters:
- ctx (*gin.Context): The Gin context object that represents the HTTP request and response.

Returns:
- None. However, it sets the "uid" key in the context object.

Example usage:
router := gin.Default()
router.Use(JwtAuthMiddleWare())

	router.GET("/protected", func(ctx *gin.Context) {
		uid := ctx.GetUint("uid")
		// Do something with uid
	})
*/
func JwtAuthMiddleWare() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		uid, err := token.ExtractTokenID(ctx)
		if err != nil {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
			return
		}
		ctx.Set("uid", uid)
		ctx.Next()
	}
}
